<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 05.08.17
 * Time: 1:39
 */

namespace common\components;


use frontend\assets\AppAsset;
use yii\base\ViewNotFoundException;

class Application extends \yii\web\Application
{
    /**
     * @return object|View
     */
    public function getView()
    {
        return $this->get('view');
    }

    public function getApplicationName()
    {
        return $this->name;
    }
}
