<?php

namespace common\models\entity\billing;

/**
 * This is the ActiveQuery class for [[BillingBalance]].
 *
 * @see BillingBalance
 */
class BillingBalanceRepository extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return BillingBalance[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return BillingBalance|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
