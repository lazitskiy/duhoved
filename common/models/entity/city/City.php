<?php

namespace common\models\entity\city;

use common\traits\base\CreatedAtBehaviorAwareTrait;

/**
 * This is the model class for table "city".
 *
 * @property integer $id
 * @property string $fias_id
 * @property string $name
 * @property string $region_fias_id
 * @property string $region_with_type
 * @property string $data
 * @property string $created_at
 * @property string $updated_at
 */
class City extends \yii\db\ActiveRecord
{
    use CreatedAtBehaviorAwareTrait;

    /**
     * @inheritdoc
     * @return CityRepository the active query used by this AR class.
     */
    public static function getRepository()
    {
        return new CityRepository(get_called_class());
    }
}
