<?php

namespace common\models\entity\category;

/**
 * This is the ActiveQuery class for [[Category]].
 *
 * @see Category
 */
class CategoryRepository extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Category[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Category|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @return array
     */
    public function getForDropdown()
    {
        $categories = $this
            ->indexBy('id')
            ->all();
        $categories = array_map(function ($el) {
            /** @var Category $el */
            return str_repeat('-', $el->parent_id * 3) . $el->name;
        }, $categories);

        return $categories;
    }
}
