<?php

namespace common\models\entity\category;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $name
 * @property string $slug
 * @property string $created_at
 * @property string $updated_at
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     * @return CategoryRepository the active query used by this AR class.
     */
    public static function getRepository()
    {
        return new CategoryRepository(get_called_class());
    }
}
