<?php
return [
    'language' => 'ru-RU',
    'timeZone' => 'UTC',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'modules' => [
        'user' => [
            'class' => \dektrium\user\Module::class,
            'modelMap' => [
                'User' => \common\models\entity\user\User::class,
            ],
            'controllerMap' => [
                'registration' => \frontend\controllers\user\RegistrationController::class,
                'settings' => \frontend\controllers\user\IndexController::class,
                'question' => \frontend\controllers\user\QuestionController::class,
            ],
            'admins' => ['lazitskiy'],
            'urlRules' => [
                '/' => 'settings/index',
                'question' => 'question/index',
            ]
            //'urlPrefix' => null
        ],
    ],
    'components' => [
        'cache' => [
            'class' => \yii\caching\FileCache::class,
        ],
        'mailer' => [
            'class' => \yii\swiftmailer\Mailer::class,
            'useFileTransport' => true,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'exchange.example.com', //вставляем имя или адрес почтового сервера
                'username' => '',
                'password' => '',
                'port' => '25',
                'encryption' => '',
            ],
        ],
        'dadataSuggestApi' => [
            'class' => \skeeks\yii2\dadataSuggestApi\DadataSuggestApi::class,
            'authorization_token' => 'e39e431365f7513da6f8bd16d5c8c23f0a0f4394',
            'timeout' => 12,
        ],
    ],
];
