<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 05.08.17
 * Time: 3:26
 */

$container = Yii::$container;

$container->setSingleton(\common\services\OrderService::class);
$container->setSingleton(\common\services\UserService::class);
$container->setSingleton(\common\services\QuestionService::class);
$container->setSingleton(\common\services\GeoService::class);
$container->setSingleton(\common\services\BillingService::class);
$container->setSingleton(\common\services\SettingService::class);
