<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 13.08.17
 * Time: 12:27
 */

namespace common\traits;


use common\services\SettingService;

trait SettingServiceAwareTrait
{
    /**
     * @return object|SettingService
     */
    public function getSettingService()
    {
        return \Yii::$container->get(SettingService::class);
    }
}
