<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 10.08.17
 * Time: 19:16
 */

namespace common\traits;


use common\services\BillingService;

trait BillingServiceAwareTrait
{
    /**
     * @return object|BillingService
     */
    public function getBillingService()
    {
        return \Yii::$container->get(BillingService::class);
    }
}
