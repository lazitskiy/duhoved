<?php

namespace common\traits\base;


trait ApplicationAwareTrait
{
    /**
     * @return \common\components\Application|\yii\web\Application
     */
    public function getApplication()
    {
        return \Yii::$app;
    }
}
