<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 05.08.17
 * Time: 15:29
 */

namespace common\services;


use common\models\entity\orderQuestion\Question;
use frontend\models\forms\order\QuestionForm;

class QuestionService extends BaseService
{
    /**
     * @param QuestionForm $questionForm
     * @param int $userId
     * @return bool|Question
     */
    public function create(QuestionForm $questionForm, int $userId, int $cityId)
    {
        $question = new Question([
            'user_id' => $userId,
            'city_id' => $cityId,
            'category_id' => $questionForm->category_id,
            'title' => $questionForm->question_title,
            'text' => $questionForm->question_text,
            'status' => Question::STATUS_NEW,
        ]);

        if (!$question->save()) {
            $this->addErrors($question->getErrors());

            return false;
        }

        return $question;
    }

    /**
     * Шаг 2. Установка стоимости
     *
     * @param Question $question
     * @param $variant
     * @param $cost
     * @return bool
     */
    public function setVariantCost(Question $question, $variant, $cost)
    {
        $question->cost = $cost;
        $question->variant = $variant;
        $question->status = Question::STATUS_WAITING_PAYMENT;
        if (!$question->save()) {
            $this->addErrors($question->getErrors());

            return false;
        }

        return true;
    }

    /**
     * @param int $questionId
     * @return Question|null
     */
    public function getById(int $questionId)
    {
        return Question::getRepository()->where(['id' => $questionId])->one();
    }

    /**
     * @param $userId
     * @return array|Question[]
     */
    public function getByUserId($userId)
    {
        $questions = Question::getRepository()
            ->where(['user_id' => $userId])
            ->orderBy(['id' => SORT_DESC])
            ->all();

        return $questions;
    }
}
