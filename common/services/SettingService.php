<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 13.08.17
 * Time: 12:26
 */

namespace common\services;


class SettingService extends BaseService
{
    /**
     * Пока все настройки будем складывать в одно место сюда
     */

    private const QUESTION_FIX_PRICE_COST = 90;
    private const QUESTION_STANDARD_COST = 299;
    private const QUESTION_EXTENDED_COST = 499;

    const USER_REGISTER_QUESTION_FIX_PRICE_SYSTEM_NAME = 'user_register_question_fix_price_setting';
    const QUESTION_VARIANTS_SYSTEM_NAME = 'question_variants';

    /**
     * @var array
     */
    private $settings;

    public function __construct()
    {
        /**
         * При регистрации даем вопрос с фиксированной ценой?
         * 1. Активна натройка/нет?
         * 2. Цена в рублях
         * 3. Переводы слов для биллинга
         */
        $this->settings[static::USER_REGISTER_QUESTION_FIX_PRICE_SYSTEM_NAME] = [
            'active' => true,
            'cost' => static::QUESTION_FIX_PRICE_COST,
            'billing_history_reason' => [
                'type' => 'gift', //real|gift
                'reason' => 'Вопрос по фиксированной цене ' . static::QUESTION_FIX_PRICE_COST . ' руб. (подарок за регистрацию)',
            ],
        ];

        /**
         * Настройка цен вариантов вопросов
         */
        $this->settings[static::QUESTION_VARIANTS_SYSTEM_NAME] = [
            'standard' => [
                'systemName' => 'standard',
                'name' => 'Базовый',
                'priceMin' => static::QUESTION_STANDARD_COST,
            ],
            'extended' => [
                'systemName' => 'extended',
                'name' => 'Расширенный',
                'priceMin' => static::QUESTION_EXTENDED_COST,
            ],
            'fix_cost' => [
                'systemName' => 'fix_cost',
                'name' => 'Подарочный тариф',
                'priceMin' => static::QUESTION_FIX_PRICE_COST,
            ],
        ];
    }


    public function get($settingSystemName)
    {
        return $this->settings[$settingSystemName] ?? ['active' => false,];
    }
}
