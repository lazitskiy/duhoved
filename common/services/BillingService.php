<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 12.08.17
 * Time: 0:08
 */

namespace common\services;


use common\components\Application;
use common\models\entity\billing\BillingBalance;
use common\models\entity\billing\BillingHistory;
use common\traits\base\ApplicationAwareTrait;
use common\traits\SettingServiceAwareTrait;

class BillingService extends BaseService
{
    use ApplicationAwareTrait;
    use SettingServiceAwareTrait;

    /**
     * @var Application
     */
    protected $application;

    /**
     * @var SettingService
     */
    protected $settingService;

    public function __construct()
    {
        $this->application = $this->getApplication();
        $this->settingService = $this->getSettingService();
    }

    /**
     * Просто создаем запись с балансом юзера
     * @param int $userId
     */
    public function initBilling(int $userId)
    {
        $billingBalance = new BillingBalance([
            'user_id' => $userId,
        ]);
        $billingBalance->save();
    }

    /**
     * Начисляем бесплатный вопрос
     * @param $userId
     */
    public function giveQuestionFixPrice(int $userId)
    {
        BillingBalance::updateAllCounters(
            ['fix_cost_question_count' => 1],
            ['user_id' => $userId]
        );
        $setting = $this->settingService->get($this->settingService::USER_REGISTER_QUESTION_FIX_PRICE_SYSTEM_NAME);
        $reason = $setting['billing_history_reason'];
        $this->addHistory($userId, 0, $reason);
    }

    /**
     * @param int $userId
     * @param int $summ
     * @param array $reason
     */
    private function addHistory(int $userId, int $summ, array $reason)
    {
        $billingHistory = new BillingHistory([
            'user_id' => $userId,
            'summ' => $summ,
            'type' => $reason['type'],
            'reason' => $reason['reason'],
        ]);
        $billingHistory->save();
    }
}
