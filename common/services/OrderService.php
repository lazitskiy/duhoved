<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 05.08.17
 * Time: 3:28
 */

namespace common\services;


use common\components\Application;
use common\models\entity\orderQuestion\Question;
use common\traits\base\ApplicationAwareTrait;
use common\traits\BillingServiceAwareTrait;
use common\traits\GeoServiceAwareTrait;
use common\traits\QuestionServiceAwareTrait;
use common\traits\SettingServiceAwareTrait;
use common\traits\UserServiceAwareTrait;
use frontend\models\forms\order\QuestionForm;
use frontend\models\forms\order\QuestionVariantForm;
use frontend\models\forms\user\OrderRegisterForm;

class OrderService extends BaseService
{
    use ApplicationAwareTrait;
    use UserServiceAwareTrait;
    use QuestionServiceAwareTrait;
    use GeoServiceAwareTrait;
    use BillingServiceAwareTrait;
    use SettingServiceAwareTrait;

    /**
     * @var Application
     */
    protected $application;

    /**
     * @var \yii\db\Connection
     */
    protected $connection;

    /**
     * @var UserService
     */
    protected $userService;

    /**
     * @var QuestionService
     */
    protected $questionService;

    /**
     * @var GeoService
     */
    protected $geoService;

    /**
     * @var BillingService
     */
    protected $billingService;

    /**
     * @var SettingService
     */
    protected $settingService;

    public function __construct()
    {
        $this->application = $this->getApplication();
        $this->connection = $this->getApplication()->getDb();
        $this->userService = $this->getUserService();
        $this->questionService = $this->getQuestionService();
        $this->geoService = $this->getGeoService();
        $this->billingService = $this->getBillingService();
        $this->settingService = $this->getSettingService();
    }

    /**
     * Форма подачи вопроса
     *
     * @param QuestionForm $questionForm
     * @param OrderRegisterForm $orderRegisterForm
     * @return bool|Question
     */
    public function saveQuestion(QuestionForm $questionForm, OrderRegisterForm $orderRegisterForm)
    {
        return $this->connection->transaction(function () use ($questionForm, $orderRegisterForm) {
            $application = $this->getApplication();

            $user = $orderRegisterForm->getUser();
            // Регистриурем
            if (!$user) {
                $user = $this->userService->register($orderRegisterForm->name, $orderRegisterForm->email, $orderRegisterForm->phone);
                if (!$user) {
                    $this->addErrors($this->userService->getErrors());

                    return false;
                }

                // Логиним юзера
                $application->getUser()->login($user, $user->module->rememberFor);
                $orderRegisterForm->setUser($user);

                // Создаем балланс
                $this->billingService->initBilling($user->getId());

                // Дарим бесплатный вопрос
                $setting = $this->settingService->get($this->settingService::USER_REGISTER_QUESTION_FIX_PRICE_SYSTEM_NAME);
                if ($setting['active']) {
                    $this->billingService->giveQuestionFixPrice($user->getId());
                }
            }

            $city = $this->geoService->saveCityFromDadataString($questionForm->getCityData());
            // TODO отправить письмо

            // делаем вопрос
            $question = $this->questionService->create($questionForm, $user->getId(), $city->id);
            if (!$question) {
                $this->addErrors($this->questionService->getErrors());

                return false;
            }

            return $question;
        });
    }

    /**
     * Шаг2. детали вопроса, оплата и кол-во человек
     *
     * @param $questionId int
     * @param $questionStep2Form QuestionVariantForm
     * @return bool
     */
    public function saveQuestionStep2(int $questionId, QuestionVariantForm $questionStep2Form)
    {
        $variant = $questionStep2Form->getChosenVariant();
        $costVariant = $variant['priceMin'];
        // Это доп услуги, пока не реализовано
        $costAdditional = 0;
        $cost = $costVariant + $costAdditional;

        // Обновляем стоимость
        $question = $this->questionService->getById($questionId);

        if (!$this->questionService->setVariantCost($question, $variant['systemName'], $cost)) {
            $this->addErrors($this->questionService->getErrors());

            return false;
        }

        // TODO письмо о принятии вопроса. Теперь он находится на модерации

        return true;
    }
}
