<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 05.08.17
 * Time: 3:28
 */

namespace common\services;


use common\traits\base\AddErrorAwareTrait;

class BaseService
{
    use AddErrorAwareTrait;
}
