<?php
/**
 * @var $this \yii\web\View
 * @var $content string
 */

use \yii\helpers\Html;
use \yii\widgets\Breadcrumbs;
use \frontend\assets\FrontendAsset;
use \common\widgets\Alert;
use \yii\helpers\Url;

$app = \Yii::$app;
FrontendAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">

    <div>
        <div>Логотип</div>
        <div>Консультация духовного практика в г. Москва</div>
        <div>
            <?php if ($app->getUser() && $app->getUser()->getIsGuest()): ?>
                <a href="<?= Url::to(['/user/security/login']) ?>">Вход</a>
            <?php else: ?>
                <a href="<?= Url::to(['/user']) ?>">Кабинет</a>
                <a href="<?= Url::to(['/user/security/logout']) ?>" data-method="post">Выйти</a>
            <?php endif; ?>
        </div>
    </div>

    <div>
        <ul>
            <li><a href="<?= Url::to(['/get/question']) ?>">Задать вопрос</a></li>
            <li><a href="">Заказать звонок</a></li>
        </ul>
    </div>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
