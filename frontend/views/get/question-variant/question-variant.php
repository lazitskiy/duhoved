<?php
/**
 * @var \yii\web\View $this
 * @var $questionVariantForm \frontend\models\forms\order\QuestionVariantForm
 */

use \common\components\ActiveForm;
use \yii\helpers\Html;

$variantStandard = $questionVariantForm->getVariantStandard();
$variantExtended = $questionVariantForm->getVariantExtended();
$variantFixCost = $questionVariantForm->getVariantFixCost();
?>

<h1>Задайте свой вопрос духовному практику</h1>

<div>
    <div>
        <? $form = ActiveForm::begin([]) ?>
        <div class="form-group">
            <h3>Укажите тип вопроса</h3>
            <div class="form-group field-questionformstep2-variant required">

                <div id="questionformstep2-variant" aria-required="true" class="clearfix">
                    <?php if ($variantExtended): ?>
                        <div class="col-md-3">
                            <label>
                                <?= Html::activeRadio($questionVariantForm, 'variant', [
                                    'value' => $variantExtended['systemName'],
                                    'label' => false,
                                    'uncheck' => false,
                                ]) ?>
                                <?= $variantExtended['name'] ?>

                                <h3>Вы получите</h3>
                                <ul>
                                    <li>Отличие 1</li>
                                    <li>Отличие 1</li>
                                    <li>Отличие 1</li>
                                </ul>
                                <p>
                                    Стоимость: <?= $variantExtended['priceMin'] ?>
                                </p>
                            </label>
                        </div>
                    <?php endif; ?>

                    <?php if ($variantStandard): ?>
                        <div class="col-md-3">
                            <label>
                                <?= Html::activeRadio($questionVariantForm, 'variant', [
                                    'value' => $variantStandard['systemName'],
                                    'label' => false,
                                    'uncheck' => false,
                                ]) ?>
                                <?= $variantStandard['name'] ?>
                                <h3>Сюда входит</h3>
                                <ul>
                                    <li>Фича 1</li>
                                    <li>Фича 1</li>
                                    <li>Фича 1</li>
                                </ul>
                                <p>
                                    Стоимость: <?= $variantStandard['priceMin'] ?>
                                </p>
                            </label>
                        </div>
                    <?php endif; ?>


                    <?php if ($variantFixCost): ?>
                        <div class="col-md-3">
                            <label>
                                <?= Html::activeRadio($questionVariantForm, 'variant', [
                                    'value' => $variantFixCost['systemName'],
                                    'label' => false,
                                    'uncheck' => false,
                                ]) ?>
                                <?= $variantFixCost['name'] ?>
                                <h3>Сюда входит</h3>
                                <ul>
                                    <li>Фича 1</li>
                                    <li>Фича 1</li>
                                    <li>Фича 1</li>
                                </ul>
                                <p>
                                    Стоимость: <?= $variantFixCost['priceMin'] ?>
                                </p>
                            </label>
                        </div>
                    <?php endif; ?>

                </div>

                <div class="help-block"></div>
            </div>
        </div>
        <div class="form-group">
            <?= Html::submitButton('Продолжить', ['class' => 'btn btn-primary']) ?>
        </div>
        <?php $form::end() ?>
    </div>
</div>
