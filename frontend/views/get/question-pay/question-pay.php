<?php
/**
 * @var \yii\web\View $this
 * @var $questionStep2Form \frontend\models\forms\order\QuestionVariantForm
 */

use \common\components\ActiveForm;
use \yii\helpers\Html;

?>

<h1>Задайте свой вопрос духовному практику</h1>

<div>
    <div>
        <? $form = ActiveForm::begin([]) ?>
        <div>
            <h3>Выберите способ оплаты</h3>

        </div>
        
        <div class="form-group">
            <?= Html::submitButton('Продолжить', ['class' => 'btn btn-primary']) ?>
        </div>
        <?php $form::end() ?>
    </div>

</div>
