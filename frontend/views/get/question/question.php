<?php
/**
 * @var \common\components\View $this
 * @var $applicationName string
 * @var $questionForm \frontend\models\forms\order\QuestionForm
 * @var $orderRegisterForm \frontend\models\forms\user\OrderRegisterForm
 * @var $categories array
 */

use \yii\helpers\Url;
use \yii\helpers\Html;
use \common\components\ActiveForm;
use \yii\jui\AutoComplete;

?>

<h1>Задайте свой вопрос духовному практику</h1>

<div>
    <div>
        Чтобы задать вопрос юристу онлайн, вам потребуется всего несколько минут.
        Наши специалисты ответят в кратчайшие сроки и подскажут, как правильно поступить
        в вашей ситуации. Доверьте решение своей проблемы квалифицированным юристам!
        <a href="<?= Url::to(['static-page/guarantees']) ?>">Политика защиты клиентов</a>
    </div>
    <div>
        Лучший сервис по качеству
        юридических услуг онлайн в 2016 г.
        по версии пользователей Рунета
    </div>
    <div>
        <ul>
            <li>Как работает <?= $applicationName ?></li>
            <li>1Задайте вопрос

                Мы получаем более 1500
                вопросов каждый день.
                Задайте свой!
            </li>
            <li>2Получите ответы

                На вопросы круглосуточно
                отвечают юристы со всей России.
                Среднее время ответа — 15 минут.
            </li>
            <li>3Проблема решена!

                95,4% клиентов остаются
                полностью довольны ответами
                и рекомендуют нас друзьям.
            </li>
        </ul>
    </div>

    <div>

        <? $form = ActiveForm::begin([]) ?>
        <h3>Ваш вопрос</h3>
        <?= $form->field($questionForm, 'category_id')->dropDownList(
            $categories,
            ['prompt' => 'Выберите категорию']
        ) ?>
        <?= $form->field($questionForm, 'question_title')->textInput() ?>
        <?= $form->field($questionForm, 'question_text')->textarea() ?>

        <h3>О вас</h3>
        <?php if ($user = $orderRegisterForm->getUser()): ?>
            <div>
                <?= $user->profile->name ?> (<?= $user->email ?>)
            </div>
            <div>
                Город:
                <?= AutoComplete::widget([
                    'value' => $questionForm->getCity()->name,
                    'clientOptions' => [
                        'source' => Url::to(['/suggest/city']),
                        'autoFill' => true,
                        'select' => new \yii\web\JsExpression("function(event, ui) {
                            $('#js-fias-data').val(JSON.stringify(ui.item.data.data));
                        }"),
                    ],
                ]); ?>
                <?= $form->field($questionForm, 'cityData')->hiddenInput(['id' => 'js-fias-data']) ?>
            </div>
        <?php else: ?>
            <?= $form->field($orderRegisterForm, 'name')->textInput() ?>
            <?= $form->field($orderRegisterForm, 'email')->textInput() ?>
            <?= $form->field($orderRegisterForm, 'password')->passwordInput() ?>
            <?= $form->field($orderRegisterForm, 'phone')->textInput() ?>
        <?php endif; ?>
        <div class="form-group">
            <?= Html::submitButton('Продолжить', ['class' => 'btn btn-primary']) ?>
        </div>
        <?php $form::end() ?>
    </div>
</div>
