<?php


use yii\helpers\Html;

/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 07.08.17
 * Time: 23:11
 * @var $this yii\web\View
 * @var $content string
 */
/** @var $user common\models\entity\user\User */
$user = \Yii::$app->getUser()->getIdentity();
?>
<?php $this->beginContent('@frontend/views/layouts/main.php') ?>

<div class="row">
    <div class="col-md-3">
        <?= $this->render('userMenu', [
            'user' => $user,
        ]) ?>
    </div>
    <div class="col-md-9">
        <?= $content ?>
    </div>
</div>

<?php $this->endContent(); ?>
