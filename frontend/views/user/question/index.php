<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 13.08.17
 * Time: 15:22
 * @var $questions \common\models\entity\orderQuestion\Question[]
 */
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Вопрос
            </div>
            <div class="panel-body">
                <?php if (!$questions): ?>
                    Вы еще не задали ни одного вопроса, начите здесь
                <?php endif; ?>

                <?php if ($questions): ?>
                    <?php foreach ($questions as $question): ?>
                        <div>
                            <div>
                                Стоимость вопроса: <?= $question->cost ?>
                            </div>
                            <div>
                                Статус: <?= $question->status ?>
                            </div>
                            <div>
                                <a href="<?= $question->getUrl() ?>"><?= $question->title ?></a>
                                <p>
                                    <?= $question->text ?>
                                </p>
                                <div>
                                    <i>Число:</i>
                                </div>
                            </div>
                            <hr>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
