<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'assetManager' => [
            'class' => \yii\web\AssetManager::class,
            'linkAssets' => false,
            'forceCopy' => true,
            'appendTimestamp' => true,
            //'bundles' => YII_ENV_DEV ? [] : require(__DIR__ . '/asset-prod.php'),
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@dektrium/user/views' => '@frontend/views/user',
                ],
            ],
        ],
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'guarantees' => 'static-page/guarantees',
                'get/question/<questionId:[\d+]>/variant' => 'get/question-variant',
                'get/question/<questionId:[\d+]>/pay' => 'get/question-pay',

                'question/<questionId:[\d+]>' => 'question/index',
            ],
        ],

    ],
    'params' => $params,
];
