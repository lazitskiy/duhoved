<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class FrontendAsset extends AssetBundle
{
    public $sourcePath = '@frontend/views';
    //public $basePath = '@webroot/assets';
    public $baseUrl = '@web/assets';

    public $publishOptions = [
        'only' => [
            '*.js',
            '*.css',
        ],
    ];

    public $css = [
    ];
    public $js = [
        'user/registration/register.js',

        'get/question-variant/question-variant.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
