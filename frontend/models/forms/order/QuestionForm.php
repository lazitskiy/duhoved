<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 05.08.17
 * Time: 1:56
 */

namespace frontend\models\forms\order;


use common\models\entity\city\City;
use common\traits\base\ApplicationAwareTrait;
use common\traits\GeoServiceAwareTrait;
use frontend\models\forms\BaseForm;

class QuestionForm extends BaseForm
{
    use ApplicationAwareTrait;
    use GeoServiceAwareTrait;

    public $category_id;
    public $city_id;
    public $question_title;
    public $question_text;
    /**
     * @var string Платный бесплатный
     */
    public $question_type;

    /**
     * @var City
     */
    protected $city;

    protected $cityData;

    public function init()
    {
        parent::init();

        $ip = $this->getApplication()->getRequest()->getUserIP();
        $this->city = $this->getGeoService()->getCityByIp($ip);

        $this->cityData = $this->city->data;
    }

    /**
     * @return City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getCityData()
    {
        return $this->cityData;
    }

    public function attributeLabels()
    {
        return [
            'category_id' => 'Укажите рубрику',
            'question_title' => 'Ваш вопрос',
            'question_text' => 'Подробное описание ситуации',
            'city' => 'Город',
            'cityData' => false,
        ];
    }

    public function attributeHints()
    {
        return [
            'question_title' => 'Например: когда у меня появятся бабки',
            'question_text' => 'Постарайтесь как можно более подробно описать вашу ситуацию',
        ];
    }

    public function rules()
    {
        return [
            ['category_id', 'required'],
            ['category_id', 'number', 'integerOnly' => true],

            ['cityData', 'required'],

            ['question_title', 'required'],
            ['question_title', 'string', 'length' => [8, 256]],

            ['question_text', 'required'],
        ];
    }
}
