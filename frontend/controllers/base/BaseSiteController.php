<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 05.08.17
 * Time: 1:24
 */

namespace frontend\controllers\base;


use common\traits\base\ApplicationAwareTrait;

class BaseSiteController extends BaseController
{
    use ApplicationAwareTrait;

    public function addFlashErrors($errors = [])
    {
        foreach ($errors as $_error) {
            foreach ($_error as $error) {
                $this->getApplication()->getSession()->addFlash('error', $error);
            }
        }
    }
}
