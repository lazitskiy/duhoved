<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 06.08.17
 * Time: 16:27
 */

namespace frontend\controllers;


use frontend\controllers\base\BaseSiteController;

class PolicyController extends BaseSiteController
{
    public function actionTerms()
    {
        return $this->render('terms');
    }

    public function actionConfidential()
    {
        return $this->render('confidential');
    }
}
