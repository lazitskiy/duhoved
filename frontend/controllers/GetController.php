<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 05.08.17
 * Time: 1:25
 */

namespace frontend\controllers;


use common\models\entity\category\Category;
use common\services\QuestionService;
use common\traits\QuestionServiceAwareTrait;
use frontend\controllers\base\BaseSiteController;
use frontend\models\forms\order\QuestionForm;
use frontend\models\forms\order\QuestionVariantForm;
use frontend\models\forms\user\OrderRegisterForm;
use common\traits\base\ApplicationAwareTrait;
use common\traits\OrderServiceAwareTrait;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use common\components\ActiveForm;

class GetController extends BaseSiteController
{
    use ApplicationAwareTrait;
    use OrderServiceAwareTrait;
    use QuestionServiceAwareTrait;

    /**
     * @var QuestionService;
     */
    protected $questionService;

    public function init()
    {
        parent::init();

        $this->questionService = $this->getQuestionService();
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    ['allow' => true, 'actions' => ['question-variant'], 'roles' => ['@']],
                    ['allow' => true, 'actions' => ['question-pay'], 'roles' => ['@', '?']],
                ],
            ],
        ];
    }

    public function actionQuestion()
    {
        $questionForm = new QuestionForm();
        $orderRegisterForm = new OrderRegisterForm();
        $request = $this->getApplication()->getRequest();
        $response = $this->getApplication()->getResponse();

        if (
            $request->getIsAjax() &&
            $questionForm->load($request->post()) &&
            $orderRegisterForm->load($request->post())
        ) {
            $response->format = Response::FORMAT_JSON;

            if (!$orderRegisterForm->getUser()) {
                $orderRegisterForm->setScenario(OrderRegisterForm::SCENARIO_USER_NEW);
            }

            return array_merge(ActiveForm::validate($questionForm), ActiveForm::validate($orderRegisterForm));
        }
        if (
            $request->getIsPost() &&
            $questionForm->load($request->post()) && $questionForm->validate() &&
            $orderRegisterForm->load($request->post()) && $orderRegisterForm->validate() &&
            $question = $this->getOrderService()->saveQuestion($questionForm, $orderRegisterForm)) {

            return $this->redirect(['/get/question-variant', 'questionId' => $question->id]);
        } else {
            $errors = array_merge(
                $questionForm->getErrors(),
                $orderRegisterForm->getErrors(),
                $this->getOrderService()->getErrors()
            );

            $this->addFlashErrors($errors);
        }

        $categories = Category::getRepository()->getForDropdown();

        return $this->render('question/question', [
            'applicationName' => $this->getApplication()->getApplicationName(),
            'questionForm' => $questionForm,
            'orderRegisterForm' => $orderRegisterForm,
            'categories' => $categories,
        ]);
    }

    /**
     * Шаг 2. Выбор варианта цены вопроса
     *
     * @param $questionId
     * @return array|string|Response
     * @throws NotFoundHttpException
     */
    public function actionQuestionVariant($questionId)
    {
        $request = $this->getApplication()->getRequest();
        $response = $this->getApplication()->getResponse();
        $question = $this->questionService->getById($questionId);

        if (!$question) {
            throw new NotFoundHttpException('Такого вопроса не существует');
        }

        $questionVariantForm = new QuestionVariantForm();
        $questionVariantForm->setAttributes($question->getAttributes());

        if ($question->user_id != $questionVariantForm->getUser()->getId()) {
            throw new NotFoundHttpException('Чужие вопросы нельзя редактировать');
        }

        if ($request->getIsAjax() && $questionVariantForm->load($request->post())) {
            $response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($questionVariantForm);
        }

        if (
            $request->getIsPost() &&
            $questionVariantForm->load($request->post()) && $questionVariantForm->validate() &&
            $this->getOrderService()->saveQuestionStep2($questionId, $questionVariantForm)
        ) {

            return $this->redirect(['/get/question-pay', 'questionId' => $questionId]);
        } else {
            $errors = array_merge(
                $questionVariantForm->getErrors(),
                $this->getOrderService()->getErrors()
            );

            $this->addFlashErrors($errors);
        }

        return $this->render('question-variant/question-variant', [
            'questionVariantForm' => $questionVariantForm,
        ]);
    }

    public function actionQuestionPay()
    {
        return $this->render('question-pay/question-pay', [
            //'questionStep2Form' => $questionStep2Form,
        ]);
    }
}
