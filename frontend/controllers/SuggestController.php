<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 10.08.17
 * Time: 21:11
 */

namespace frontend\controllers;


use common\traits\base\ApplicationAwareTrait;
use common\traits\GeoServiceAwareTrait;
use yii\web\Controller;
use yii\web\Response;

class SuggestController extends Controller
{
    use ApplicationAwareTrait;
    use GeoServiceAwareTrait;

    public function actionCity($term)
    {
        $this->getApplication()->getResponse()->format = Response::FORMAT_JSON;

        $suggest = $this->getGeoService()->getCitySuggest($term);

        $data = [];
        foreach ($suggest['suggestions'] as $suggestion) {
            if (!$suggestion['data']['city_with_type']) {
                //continue;
            }
            $data[] = [
                'id' => $suggestion['data']['city_fias_id'],
                //'label' => $suggestion['data']['city_with_type'],
                'label' => $suggestion['unrestricted_value'],
                'data' => $suggestion,
            ];
        }

        return $data;

    }
}
