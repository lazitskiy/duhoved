<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 06.08.17
 * Time: 16:08
 */

namespace frontend\controllers\user;


use common\traits\base\ApplicationAwareTrait;
use common\traits\base\JavascriptAwareTrait;
use frontend\models\forms\user\RegistrationForm;
use yii\web\NotFoundHttpException;

class RegistrationController extends \dektrium\user\controllers\RegistrationController
{
    use ApplicationAwareTrait;

    public function actionRegister()
    {
        if (!$this->module->enableRegistration) {
            throw new NotFoundHttpException();
        }

        /** @var RegistrationForm $registrationForm */
        $registrationForm = \Yii::createObject(RegistrationForm::class);
        $event = $this->getFormEvent($registrationForm);

        $this->trigger(self::EVENT_BEFORE_REGISTER, $event);

        $this->performAjaxValidation($registrationForm);

        if ($registrationForm->load(\Yii::$app->request->post()) && $registrationForm->register()) {
            $this->trigger(self::EVENT_AFTER_REGISTER, $event);

            return $this->render('/message', [
                'title' => \Yii::t('user', 'Your account has been created'),
                'module' => $this->module,
            ]);
        }

        return $this->render('register', [
            'registrationForm' => $registrationForm,
            'module' => $this->module,
        ]);
    }
}
