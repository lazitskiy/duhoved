<?php
/**
 * Created by PhpStorm.
 * User: vaso
 * Date: 13.08.17
 * Time: 15:14
 */

namespace frontend\controllers\user;


use common\services\QuestionService;
use common\traits\base\UserAwareTrait;
use common\traits\QuestionServiceAwareTrait;
use yii\filters\AccessControl;

class QuestionController extends BaseUserController
{
    use QuestionServiceAwareTrait;
    use UserAwareTrait;

    /**
     * @var QuestionService
     */
    protected $questionService;

    public function init()
    {
        parent::init();

        $this->questionService = $this->getQuestionService();
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $questions = $this->questionService->getByUserId($this->getUserIdentity()->getId());

        return $this->render('/question/index', [
            'questions' => $questions,
        ]);
    }
}
